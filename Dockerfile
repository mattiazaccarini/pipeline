FROM maven:3.6.3

WORKDIR /home/pipelines

COPY target/pipelines-*.jar .
