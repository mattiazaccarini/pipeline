/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author zackm
 */
public class MainTest {
    
    public MainTest() {
    }

    /**
     * Test of addizione method, of class Main.
     */
    @org.junit.jupiter.api.Test
    public void testAddizione() {
        System.out.println("addizione");
        int a = 4;
        int b = 5;
        int expResult = 9;
        int result = Main.addizione(a, b);
        assertEquals(expResult, result);
    }
    
    @org.junit.jupiter.api.Test
    public void testSottrazione() {
        System.out.println("addizione");
        int a = 6;
        int b = 5;
        int expResult = 1;
        int result = Main.sottrazione(a, b);
        assertEquals(expResult, result);
    }
    
}
